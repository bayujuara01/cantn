package com.ariefyantobayu.modules.login;

import com.ariefyantobayu.base.BaseContract;

public interface LoginContract extends BaseContract {
    interface View extends BaseContract.View {
        void whenLoginSuccess(String token);
        void whenLoginFailure();
    }

    interface Presenter extends BaseContract.Presenter<View> {
        void actionLogin(String username, String password);
    }
}
