package com.ariefyantobayu.modules.login;

import com.ariefyantobayu.base.BasePresenter;
import com.ariefyantobayu.data.auth.AuthRepository;
import com.ariefyantobayu.data.auth.model.Auth;
import com.ariefyantobayu.data.auth.model.AuthResponse;
import com.ariefyantobayu.data.auth.remote.AuthRemoteRepository;
import com.ariefyantobayu.services.AuthService;
import com.ariefyantobayu.services.ServiceGenerator;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class LoginPresenter extends BasePresenter<LoginContract.View> implements LoginContract.Presenter {

    private final AuthRepository authRepository;

    public LoginPresenter(AuthRepository authRepository) {
        this.authRepository = authRepository;
    }

    @Override
    public void actionLogin(String username, String password) {
        Auth credentials = new Auth(username,password);
        authRepository.requestAuthenticate(credentials, new AuthRepository.AuthenticateCallback() {
            @Override
            public void onSuccess(AuthResponse data, int statusCode) {
                if (statusCode >= 400) {
                    view.whenLoginFailure();
                } else {
                    view.whenLoginSuccess(data.getData());
                }
            }

            @Override
            public void onFailed(String errorMessage) {
                view.whenLoginFailure();
            }
        });
    }
}
