package com.ariefyantobayu.modules.login;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import com.ariefyantobayu.Injector;
import com.ariefyantobayu.NavigationActivity;
import com.ariefyantobayu.R;
import com.auth0.android.jwt.Claim;
import com.auth0.android.jwt.JWT;

import java.util.ArrayList;
import java.util.Map;

public class LoginActivity extends AppCompatActivity implements LoginContract.View {

    private EditText tvUsername;
    private EditText tvPassword;
    private Button btnLogin;

    private LoginContract.Presenter presenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        presenter = new LoginPresenter(
                Injector.injectRepository(getSharedPreferences("app", Context.MODE_PRIVATE)));
        presenter.attach(this);

        tvUsername = findViewById(R.id.tv_username);
        tvPassword = findViewById(R.id.tv_password);
        btnLogin = findViewById(R.id.btn_login);

        btnLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                presenter.actionLogin(tvUsername.getText().toString(), tvPassword.getText().toString());
            }
        });
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        presenter.detach();
    }

    @Override
    public void whenLoginSuccess(String token) {
        JWT jwt = new JWT(token);
        Map<String, Claim> allClaims = jwt.getClaims();
        String name = allClaims.get("name").asString();
        String email = allClaims.get("email").asString();
        String role = allClaims.get("roles").asArray(String.class)[0];
        Log.i("whenLoginSuccess", allClaims.get("roles").asArray(String.class)[0]);

        SharedPreferences sharedPreferences = getSharedPreferences("app", Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString("token", token);
        editor.putString("role", role);
        editor.putString("name", name);
        editor.putString("email", email);
        editor.commit();

        Toast.makeText(this, "Login Successfully", Toast.LENGTH_SHORT).show();
        Intent navigationIntent = new Intent(this, NavigationActivity.class);
        startActivity(navigationIntent);
        finish();
    }

    @Override
    public void whenLoginFailure() {
        Toast.makeText(this, "Login Failure", Toast.LENGTH_SHORT).show();
    }

}
