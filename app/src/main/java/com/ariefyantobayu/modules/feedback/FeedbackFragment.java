package com.ariefyantobayu.modules.feedback;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.widget.Toast;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import com.ariefyantobayu.Injector;
import com.ariefyantobayu.R;
import com.ariefyantobayu.data.question.model.Question;
import com.ariefyantobayu.modules.feedback.adapter.FeedbackListAdapter;

import java.util.List;

public class FeedbackFragment extends Fragment implements FeedbackContract.View {

    private FeedbackContract.Presenter presenter;
    private RecyclerView rvFeedback;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        SharedPreferences sharedPreferences = getActivity().getSharedPreferences("app", Context.MODE_PRIVATE);
        presenter = new FeedbackPresenter(
                Injector.injectQuestionRepository(sharedPreferences)
        );

        presenter.attach(this);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_feedback, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        rvFeedback = view.findViewById(R.id.rv_feedback);

    }

    @Override
    public void whenFindAllQuestionSuccess(List<Question> questions) {
        if (questions.size() > 0) {
            rvFeedback.setLayoutManager(new LinearLayoutManager(getContext()));
            rvFeedback.setHasFixedSize(true);


            FeedbackListAdapter feedbackListAdapter = new FeedbackListAdapter(questions);
            rvFeedback.setAdapter(feedbackListAdapter);
        Toast.makeText(getActivity(), questions.get(0).getTitle(), Toast.LENGTH_SHORT).show();

        }
    }

    @Override
    public void whenFindAllQuestionFailure(String errorMessage) {
        Toast.makeText(getActivity(), "Get Question Failed", Toast.LENGTH_SHORT).show();
    }
}