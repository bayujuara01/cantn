package com.ariefyantobayu.modules.feedback;

import com.ariefyantobayu.base.BasePresenter;
import com.ariefyantobayu.data.question.QuestionRepository;
import com.ariefyantobayu.data.question.model.Question;

import java.util.List;

public class FeedbackPresenter extends BasePresenter<FeedbackContract.View> implements FeedbackContract.Presenter {
    private final QuestionRepository questionRepository;

    public FeedbackPresenter(QuestionRepository questionRepository) {
        this.questionRepository = questionRepository;
    }

    @Override
    public void attach(FeedbackContract.View view) {
        super.attach(view);
        questionRepository.getAllQuestion(new QuestionRepository.AllQuestionCallback() {
            @Override
            public void onSuccess(List<Question> questions) {
                view.whenFindAllQuestionSuccess(questions);
            }

            @Override
            public void onFailed(String errorMessage) {
                view.whenFindAllQuestionFailure(errorMessage);
            }
        });
    }
}
