package com.ariefyantobayu.modules.feedback;

import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import androidx.viewpager2.widget.ViewPager2;
import com.ariefyantobayu.R;
import com.ariefyantobayu.modules.feedback.adapter.FeedbackViewPagerAdapter;
import com.google.android.material.tabs.TabLayout;
import com.google.android.material.tabs.TabLayoutMediator;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

public class FeedbackMainFragment extends Fragment {

    private final String[] TAB_TITLES = new String[] {
            "Feedback", "Complaint"
    };

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_feedback_main, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ViewPager2 viewPagerMainFeedback = view.findViewById(R.id.vp_main_feedback);
        TabLayout tabMainFeedback = view.findViewById(R.id.tab_main_feedback);
        FeedbackViewPagerAdapter feedbackViewPagerAdapter = new FeedbackViewPagerAdapter(
                getChildFragmentManager(), getLifecycle());
        feedbackViewPagerAdapter.addFragment(new FeedbackFragment());
        feedbackViewPagerAdapter.addFragment(new ComplaintFragment());

        viewPagerMainFeedback.setAdapter(feedbackViewPagerAdapter);
        new TabLayoutMediator(tabMainFeedback, viewPagerMainFeedback, new TabLayoutMediator.TabConfigurationStrategy() {
            @Override
            public void onConfigureTab(@NonNull TabLayout.Tab tab, int position) {
                tab.setText(TAB_TITLES[position]);
            }
        }).attach();
    }
}