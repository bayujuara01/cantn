package com.ariefyantobayu.modules.feedback.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import com.ariefyantobayu.R;
import com.ariefyantobayu.data.question.model.InputType;
import com.ariefyantobayu.data.question.model.Question;

import java.util.List;

public class FeedbackListAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private static final int INPUT_TYPE_TEXT = 1;
    private static final int INPUT_TYPE_CHECKBOX = 2;
    private static final int INPUT_TYPE_DROPDOWN = 3;
    private static final int INPUT_TYPE_RADIO = 4;
    private static final int INPUT_TYPE_CHIP = 5;

    private final List<Question> questions;

    public FeedbackListAdapter(List<Question> questions) {
        this.questions = questions;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view;
        Context context = parent.getContext();

        switch (viewType) {
            case INPUT_TYPE_CHECKBOX:
                view = LayoutInflater.from(context).inflate(R.layout.card_item_feedback_checkbox, parent, false);
                return new TypeCheckboxViewHolder(view);
            case INPUT_TYPE_RADIO:
                view = LayoutInflater.from(context).inflate(R.layout.card_item_feedback_radio, parent, false);
                return new TypeRadioViewHolder(view);
            default:
                view = LayoutInflater.from(context).inflate(R.layout.card_item_feedback_text, parent, false);
                return new TypeTextViewHolder(view);
        }
    }

    @Override
    public int getItemViewType(int position) {
        InputType inputType = questions.get(position).getInputType();
        switch (inputType.getId()) {
            case INPUT_TYPE_CHECKBOX:
                return INPUT_TYPE_CHECKBOX;
            case INPUT_TYPE_RADIO:
                return INPUT_TYPE_RADIO;
            default:
                return INPUT_TYPE_TEXT;
        }
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
        switch (getItemViewType(position)) {
            case INPUT_TYPE_CHECKBOX:
                TypeCheckboxViewHolder typeCheckboxViewHolder = (TypeCheckboxViewHolder) holder;
                break;
            case INPUT_TYPE_RADIO:
                TypeRadioViewHolder typeRadioViewHolder = (TypeRadioViewHolder) holder;
                break;
            default:
                TypeTextViewHolder typeTextViewHolder = (TypeTextViewHolder) holder;
                break;
        }
    }

    @Override
    public int getItemCount() {
        return questions.size();
    }

    class TypeTextViewHolder extends RecyclerView.ViewHolder {

        public TypeTextViewHolder(@NonNull View itemView) {
            super(itemView);

            TextView tvText = itemView.findViewById(R.id.tv_text);
            tvText.setText("SETTER INPUT TYPE TEXT");
        }
    }

    class TypeCheckboxViewHolder extends RecyclerView.ViewHolder {

        public TypeCheckboxViewHolder(@NonNull View itemView) {
            super(itemView);

            TextView tvCheckbox = itemView.findViewById(R.id.tv_checkbox);
            tvCheckbox.setText("SETTER INPUT TYPE CHECKBOX");
        }
    }

    class TypeRadioViewHolder extends RecyclerView.ViewHolder {

        public TypeRadioViewHolder(@NonNull View itemView) {
            super(itemView);

            TextView tvRadio = itemView.findViewById(R.id.tv_radio);
            tvRadio.setText("SETTER INPUT TYPE RADIO");
        }
    }

    class TypeDropdownViewHolder extends RecyclerView.ViewHolder {

        public TypeDropdownViewHolder(@NonNull View itemView) {
            super(itemView);
        }
    }

    class TypeChipViewHolder extends RecyclerView.ViewHolder {

        public TypeChipViewHolder(@NonNull View itemView) {
            super(itemView);
        }
    }
}
