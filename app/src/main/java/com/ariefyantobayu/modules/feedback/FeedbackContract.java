package com.ariefyantobayu.modules.feedback;

import com.ariefyantobayu.base.BaseContract;
import com.ariefyantobayu.data.question.model.Question;

import java.util.List;

public interface FeedbackContract {
    interface View extends BaseContract.View {
        void whenFindAllQuestionSuccess(List<Question> questions);
        void whenFindAllQuestionFailure(String errorMessage);
    }

    interface Presenter extends BaseContract.Presenter<View> {

    }
}
