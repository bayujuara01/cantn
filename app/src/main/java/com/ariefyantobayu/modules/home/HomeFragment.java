package com.ariefyantobayu.modules.home;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.util.Log;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import com.ariefyantobayu.Injector;
import com.ariefyantobayu.R;
import com.ariefyantobayu.utils.QRCodeUtil;
import com.bumptech.glide.Glide;

import java.time.Instant;
import java.util.Date;
import java.util.Timer;
import java.util.TimerTask;

public class HomeFragment extends Fragment implements HomeContract.View {

    private ImageView imgQRCode;
    private TextView tvTimer;
    private Timer timer;
    private HomeContract.Presenter presenter;

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
        SharedPreferences sharedPreferences = context.getSharedPreferences("app", Context.MODE_PRIVATE);
        String name = sharedPreferences.getString("name", "");
        presenter = new HomePresenter(
                Injector.injectQRCodeRepository(sharedPreferences)
        );
        presenter.attach(this);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_home, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        Button btnGenerate = view.findViewById(R.id.btn_generate);
        imgQRCode = view.findViewById(R.id.img_qrcode);
        tvTimer = view.findViewById(R.id.tv_timer);

        btnGenerate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                presenter.actionGenerateCode(2);
            }
        });
    }

    @Override
    public void whenGenerateSuccess(String generatedCode) {
        Log.i("HomeFragment", generatedCode);

        try {
            Bitmap qrcodeBitmap = QRCodeUtil.generateQRCodeBitmap(generatedCode);
            Glide.with(getView()).load(qrcodeBitmap).into(imgQRCode);

            CountDownTimer qrcodeCountdown = new CountDownTimer(300000, 1000) {
                @Override
                public void onTick(long millisUntilFinished) {
                    long minute = millisUntilFinished / 60 / 1000;
                    long second = (millisUntilFinished / 1000) % 60;
                    tvTimer.setText(String.format("%d:%d", minute, second));
                }

                @Override
                public void onFinish() {

                }
            };

            qrcodeCountdown.start();

        } catch (Exception e) {
            Toast.makeText(getActivity(), "Generate QR Code Exception", Toast.LENGTH_SHORT).show();
        }

        Toast.makeText(getActivity(), "Generate QR Code Success", Toast.LENGTH_SHORT).show();
    }

    @Override
    public void whenGenerateFailure() {
        Log.i("HomeFragment", "Error Get Code");
        Toast.makeText(getActivity(), "Generate QR Code Failure", Toast.LENGTH_SHORT).show();
    }

}