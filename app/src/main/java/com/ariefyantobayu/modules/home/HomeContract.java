package com.ariefyantobayu.modules.home;

import com.ariefyantobayu.base.BaseContract;

public interface HomeContract {
    interface View extends BaseContract.View {
        void whenGenerateSuccess(String generatedCode);
        void whenGenerateFailure();
    }

    interface Presenter extends BaseContract.Presenter<View> {
        void actionGenerateCode(int idEmployee);
    }
}
