package com.ariefyantobayu.modules.home;

import android.util.Log;
import com.ariefyantobayu.base.BasePresenter;
import com.ariefyantobayu.base.BaseResponse;
import com.ariefyantobayu.data.qrcode.QRCodeRepository;
import com.ariefyantobayu.data.qrcode.model.QRCodeResponse;

public class HomePresenter extends BasePresenter<HomeContract.View> implements HomeContract.Presenter {

    private final QRCodeRepository qrCodeRepository;

    public HomePresenter(QRCodeRepository qrCodeRepository) {
        this.qrCodeRepository = qrCodeRepository;
    }

    @Override
    public void actionGenerateCode(int idEmployee) {
        qrCodeRepository.requestGenerateCode(idEmployee, new QRCodeRepository.QRGenerateCallback() {
            @Override
            public void onSuccess(BaseResponse<QRCodeResponse> data, int statusCode) {
                Log.i(HomePresenter.class.getName(), "Status Code " + String.valueOf(statusCode));
                if (statusCode < 400) {
                    QRCodeResponse qrcode = data.getData();
                    view.whenGenerateSuccess(qrcode.getGeneratedCode());
                } else {
                    view.whenGenerateFailure();
                }
            }

            @Override
            public void onFailed(String errorMessage) {
                Log.i(HomePresenter.class.getName(), errorMessage);
                view.whenGenerateFailure();
            }
        });
    }
}
