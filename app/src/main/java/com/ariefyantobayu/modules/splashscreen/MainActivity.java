package com.ariefyantobayu.modules.splashscreen;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Handler;
import android.os.Looper;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import com.ariefyantobayu.NavigationActivity;
import com.ariefyantobayu.R;
import com.ariefyantobayu.modules.login.LoginActivity;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        SharedPreferences sharedPreferences = getSharedPreferences("app", Context.MODE_PRIVATE);

        new Handler(Looper.getMainLooper()).postDelayed(new Runnable() {
            @Override
            public void run() {

                if (!sharedPreferences.getString("token", "-").equals("-")) {
                    Intent intent = new Intent(MainActivity.this, NavigationActivity.class);
                startActivity(intent);
                finish();

                } else {
                    Intent intent = new Intent(MainActivity.this, LoginActivity.class);
                    startActivity(intent);
                    finish();
                }
            }
        }, 1500);

    }
}