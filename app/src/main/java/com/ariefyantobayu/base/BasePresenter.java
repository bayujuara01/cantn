package com.ariefyantobayu.base;

public abstract class BasePresenter<V extends BaseContract.View> implements BaseContract.Presenter<V> {
    protected V view = null;

    @Override
    public void attach(V view) {
        this.view = view;
    }

    @Override
    public void detach() {
        view = null;
    }
}
