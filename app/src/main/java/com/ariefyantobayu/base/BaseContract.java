package com.ariefyantobayu.base;

public interface BaseContract {
    interface View {}
    interface Presenter<V extends View> {
        void attach(V view);
        void detach();
    }
}
