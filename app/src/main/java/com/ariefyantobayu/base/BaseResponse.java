package com.ariefyantobayu.base;

import com.google.gson.annotations.SerializedName;

public class BaseResponse<Res> {
    @SerializedName("data")
    private Res data;

    @SerializedName("message")
    private String message;

    @SerializedName("status")
    private int status;

    public BaseResponse(){}

    public BaseResponse(Res data, String message, int status) {
        this.data = data;
        this.message = message;
        this.status = status;
    }

    public Res getData(){
        return data;
    }

    public String getMessage(){
        return message;
    }

    public int getStatus(){
        return status;
    }

    public void setData(Res data) {
        this.data = data;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public void setStatus(int status) {
        this.status = status;
    }
}
