package com.ariefyantobayu.utils;

import android.graphics.Bitmap;
import com.google.zxing.BarcodeFormat;
import com.google.zxing.MultiFormatWriter;
import com.google.zxing.WriterException;
import com.google.zxing.common.BitMatrix;

import static android.graphics.Color.BLACK;
import static android.graphics.Color.WHITE;

public class QRCodeUtil {
    private static final int QRCODE_WIDTH = 400;
    private static final int QRCODE_HEIGHT = 400;
    public static Bitmap generateQRCodeBitmap(String dataString) throws WriterException {
        MultiFormatWriter multiFormatWriter;
        BitMatrix bitMatrix;
        Bitmap qrCodeBitmap;

            multiFormatWriter = new MultiFormatWriter();
            bitMatrix = multiFormatWriter.encode(dataString, BarcodeFormat.QR_CODE, QRCODE_WIDTH, QRCODE_HEIGHT);
            int width = bitMatrix.getWidth();
            int height = bitMatrix.getHeight();

            int[] pixels = new int[width * height];
            for (int y = 0; y < height; y++) {
                int offset = y * width;
                for (int x = 0; x < width; x++) {
                    pixels[offset + x] = bitMatrix.get(x, y) ? BLACK : WHITE;
                }
            }
            qrCodeBitmap = Bitmap.createBitmap(width, height, Bitmap.Config.ARGB_8888);
            qrCodeBitmap.setPixels(pixels, 0, width, 0, 0, width, height);
            return qrCodeBitmap;
    }
}
