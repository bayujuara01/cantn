package com.ariefyantobayu.data.qrcode;

import com.ariefyantobayu.base.BaseResponse;
import com.ariefyantobayu.data.qrcode.model.QRCodeRequest;
import com.ariefyantobayu.data.qrcode.model.QRCodeResponse;
import com.ariefyantobayu.data.qrcode.remote.QRCodeRemoteRepository;

public class QRCodeDataSource implements QRCodeRepository{

    private QRCodeRemoteRepository remoteDataSource;

    public QRCodeDataSource(QRCodeRemoteRepository remoteDataSource) {
        this.remoteDataSource = remoteDataSource;
    }

    @Override
    public void requestGenerateCode(int idEmployee, QRGenerateCallback callback) {
        remoteDataSource.requestGenerateCode(idEmployee, new QRGenerateCallback() {
            @Override
            public void onSuccess(BaseResponse<QRCodeResponse> data, int statusCode) {
                callback.onSuccess(data, statusCode);
            }

            @Override
            public void onFailed(String errorMessage) {
                callback.onFailed(errorMessage);
            }
        });
    }
}
