package com.ariefyantobayu.data.qrcode.model;

import com.google.gson.annotations.SerializedName;

public class QRCodeRequest {
    @SerializedName("idEmployee")
    private int idEmployee;

    public QRCodeRequest() {}

    public QRCodeRequest(int idEmployee) {
        this.idEmployee = idEmployee;
    }

    public int getIdEmployee() {
        return idEmployee;
    }
}
