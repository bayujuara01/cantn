package com.ariefyantobayu.data.qrcode.model;

import com.google.gson.annotations.SerializedName;

public class QRCodeResponse {

	@SerializedName("generatedCode")
	private String generatedCode;

	@SerializedName("id")
	private int id;

	@SerializedName("expireAt")
	private long expireAt;

	@SerializedName("createAt")
	private long createAt;

	public String getGeneratedCode(){
		return generatedCode;
	}

	public int getId(){
		return id;
	}

	public long getExpireAt(){
		return expireAt;
	}

	public long getCreateAt(){
		return createAt;
	}
}