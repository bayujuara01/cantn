package com.ariefyantobayu.data.qrcode.remote;

import android.content.SharedPreferences;
import android.util.Log;
import com.ariefyantobayu.base.BaseResponse;
import com.ariefyantobayu.data.qrcode.QRCodeRepository;
import com.ariefyantobayu.data.qrcode.model.QRCodeRequest;
import com.ariefyantobayu.data.qrcode.model.QRCodeResponse;
import com.ariefyantobayu.services.QRCodeService;
import com.ariefyantobayu.services.ServiceGenerator;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class QRCodeRemoteRepository implements QRCodeRepository {

    private final QRCodeService qrCodeService;
    private final SharedPreferences sharedPreferences;

    public QRCodeRemoteRepository(SharedPreferences sharedPreferences) {
        this.sharedPreferences = sharedPreferences;
        qrCodeService = ServiceGenerator.createWithAuthorization(
                QRCodeService.class,
                sharedPreferences.getString("token", ""));
    }

    @Override
    public void requestGenerateCode(int idEmployee, QRGenerateCallback callback) {
        QRCodeRequest qrCodeRequest = new QRCodeRequest(idEmployee);
        Call<BaseResponse<QRCodeResponse>> callQRCode = qrCodeService.generate(qrCodeRequest);
        callQRCode.enqueue(new Callback<BaseResponse<QRCodeResponse>>() {
            @Override
            public void onResponse(Call<BaseResponse<QRCodeResponse>> call, Response<BaseResponse<QRCodeResponse>> response) {
                Log.i(QRCodeRemoteRepository.class.getName(), "Berhasil");
                callback.onSuccess(response.body(), response.code());
            }

            @Override
            public void onFailure(Call<BaseResponse<QRCodeResponse>> call, Throwable t) {
                callback.onFailed(t.getMessage());
            }
        });
    }
}
