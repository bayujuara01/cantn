package com.ariefyantobayu.data.qrcode;

import com.ariefyantobayu.base.BaseResponse;
import com.ariefyantobayu.data.auth.model.AuthResponse;
import com.ariefyantobayu.data.qrcode.model.QRCodeRequest;
import com.ariefyantobayu.data.qrcode.model.QRCodeResponse;

public interface QRCodeRepository {

    void requestGenerateCode(int idEmployee, QRGenerateCallback callback);

    interface QRGenerateCallback {
        void onSuccess(BaseResponse<QRCodeResponse> data, int statusCode);
        void onFailed(String errorMessage);
    }
}
