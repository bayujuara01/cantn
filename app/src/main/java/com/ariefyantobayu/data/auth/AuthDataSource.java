package com.ariefyantobayu.data.auth;

import com.ariefyantobayu.data.auth.model.Auth;
import com.ariefyantobayu.data.auth.model.AuthResponse;
import com.ariefyantobayu.data.auth.remote.AuthRemoteRepository;

public class AuthDataSource implements AuthRepository {

    private AuthRemoteRepository remoteDataSource;

    public AuthDataSource(AuthRemoteRepository remoteDataSource) {
        this.remoteDataSource = remoteDataSource;
    }

    @Override
    public void requestAuthenticate(Auth credential, AuthenticateCallback callback) {
        remoteDataSource.requestAuthenticate(credential, new AuthenticateCallback() {
            @Override
            public void onSuccess(AuthResponse data, int statusCode) {
                callback.onSuccess(data, statusCode);
            }

            @Override
            public void onFailed(String errorMessage) {
                callback.onFailed(errorMessage);
            }
        });
    }
}
