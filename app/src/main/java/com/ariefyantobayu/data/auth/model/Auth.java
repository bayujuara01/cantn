package com.ariefyantobayu.data.auth.model;

public class Auth {
    private String username;
    private String password;

    public Auth() {};

    public Auth(String username, String password) {
        this.username = username;
        this.password = password;
    }

    public String getUsername() {
        return username;
    }

    public String getPassword() {
        return password;
    }
}
