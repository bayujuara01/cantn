package com.ariefyantobayu.data.auth;

import com.ariefyantobayu.data.auth.model.Auth;
import com.ariefyantobayu.data.auth.model.AuthResponse;

public interface AuthRepository {

    void requestAuthenticate(Auth credential, AuthenticateCallback callback);

    interface AuthenticateCallback {
        void onSuccess(AuthResponse data, int statusCode);
        void onFailed(String errorMessage);
    }
}
