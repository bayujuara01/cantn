package com.ariefyantobayu.data.auth.remote;

import android.content.SharedPreferences;
import com.ariefyantobayu.data.auth.AuthRepository;
import com.ariefyantobayu.data.auth.model.Auth;
import com.ariefyantobayu.data.auth.model.AuthResponse;
import com.ariefyantobayu.services.AuthService;
import com.ariefyantobayu.services.ServiceGenerator;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class AuthRemoteRepository implements AuthRepository {

    private final AuthService authService;
    private final SharedPreferences sharedPreferences;

    public AuthRemoteRepository(SharedPreferences sharedPreferences) {
        this.sharedPreferences = sharedPreferences;
        authService = ServiceGenerator.createWithAuthorization(AuthService.class,sharedPreferences.getString("token", ""));
    }

    @Override
    public void requestAuthenticate(Auth credential, AuthenticateCallback callback) {
        Call<AuthResponse> authCall = authService.authenticate(credential);
        authCall.enqueue(new Callback<AuthResponse>() {
            @Override
            public void onResponse(Call<AuthResponse> call, Response<AuthResponse> response) {
                callback.onSuccess(response.body(), response.code());
            }

            @Override
            public void onFailure(Call<AuthResponse> call, Throwable t) {
                callback.onFailed(t.getMessage());
            }
        });
    }
}
