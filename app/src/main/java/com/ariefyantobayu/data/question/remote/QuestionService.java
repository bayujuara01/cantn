package com.ariefyantobayu.data.question.remote;

import com.ariefyantobayu.base.BaseResponse;
import com.ariefyantobayu.data.question.model.Question;
import retrofit2.Call;
import retrofit2.http.GET;

import java.util.List;

public interface QuestionService {
    @GET("questions")
    Call<BaseResponse<List<Question>>> findAll();
}
