package com.ariefyantobayu.data.question.model;

import com.google.gson.annotations.SerializedName;

public class InputType {
    @SerializedName("id")
    private int id;
    @SerializedName("typeName")
    private String typeName;

    public int getId() {
        return id;
    }

    public String getTypeName() {
        return typeName;
    }
}
