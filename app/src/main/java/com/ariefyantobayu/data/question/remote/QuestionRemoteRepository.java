package com.ariefyantobayu.data.question.remote;

import android.content.SharedPreferences;
import com.ariefyantobayu.base.BaseResponse;
import com.ariefyantobayu.data.question.QuestionRepository;
import com.ariefyantobayu.data.question.model.Question;
import com.ariefyantobayu.services.ServiceGenerator;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import java.util.ArrayList;
import java.util.List;

public class QuestionRemoteRepository implements QuestionRepository {

    private final QuestionService questionService;
    private final SharedPreferences sharedPreferences;

    public QuestionRemoteRepository(SharedPreferences sharedPreferences) {
        this.sharedPreferences = sharedPreferences;
        questionService = ServiceGenerator.createWithAuthorization(
                QuestionService.class,
                sharedPreferences.getString("token", "")
        );
    }

    @Override
    public void getAllQuestion(AllQuestionCallback callback) {
        Call<BaseResponse<List<Question>>> callAllQuestion = questionService.findAll();
        callAllQuestion.enqueue(new Callback<BaseResponse<List<Question>>>() {
            @Override
            public void onResponse(Call<BaseResponse<List<Question>>> call,
                                   Response<BaseResponse<List<Question>>> response) {
                List<Question> questions = new ArrayList<>();
                if (response.body() != null) {
                    questions = response.body().getData();
                }

                if (response.isSuccessful()) {
                    callback.onSuccess(questions);
                } else {
                    callback.onFailed(response.errorBody().toString());
                }
            }

            @Override
            public void onFailure(Call<BaseResponse<List<Question>>> call, Throwable t) {
                callback.onFailed(t.getMessage());
            }
        });
    }

    @Override
    public void getDetailQuestion() {

    }
}
