package com.ariefyantobayu.data.question;

import com.ariefyantobayu.data.question.model.Question;
import com.ariefyantobayu.data.question.remote.QuestionRemoteRepository;

import java.util.List;

public class QuestionDataSource implements QuestionRepository {

    private final QuestionRemoteRepository questionRemoteRepository;

    public QuestionDataSource(QuestionRemoteRepository questionRemoteRepository) {
        this.questionRemoteRepository = questionRemoteRepository;
    }

    @Override
    public void getAllQuestion(AllQuestionCallback callback) {
        questionRemoteRepository.getAllQuestion(new AllQuestionCallback() {
            @Override
            public void onSuccess(List<Question> questions) {
                callback.onSuccess(questions);
            }

            @Override
            public void onFailed(String errorMessage) {
                callback.onFailed(errorMessage);
            }
        });
    }

    @Override
    public void getDetailQuestion() {

    }
}
