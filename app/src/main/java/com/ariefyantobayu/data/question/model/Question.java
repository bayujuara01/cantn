package com.ariefyantobayu.data.question.model;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class Question {

    @SerializedName("id")
    private int id;
    @SerializedName("title")
    private String title;
    @SerializedName("subtext")
    private String subtext;
    @SerializedName("type")
    private int type;
    @SerializedName("typeName")
    private String typeName;
    @SerializedName("inputType")
    private InputType inputType;
    @SerializedName("options")
    private List<Option> options;

    public int getId() {
        return id;
    }

    public String getTitle() {
        return title;
    }

    public String getSubtext() {
        return subtext;
    }

    public int getType() {
        return type;
    }

    public String getTypeName() {
        return typeName;
    }

    public InputType getInputType() {
        return inputType;
    }

    public List<Option> getOptions() {
        return options;
    }
}
