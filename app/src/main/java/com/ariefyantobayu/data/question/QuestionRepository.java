package com.ariefyantobayu.data.question;

import com.ariefyantobayu.data.question.model.Question;

import java.util.List;

public interface QuestionRepository {
    void getAllQuestion(AllQuestionCallback callback);
    void getDetailQuestion();

    interface AllQuestionCallback {
        void onSuccess(List<Question> questions);
        void onFailed(String errorMessage);
    }

    interface QuestionCallback {

    }
}
