package com.ariefyantobayu;

import android.content.SharedPreferences;
import com.ariefyantobayu.data.auth.AuthDataSource;
import com.ariefyantobayu.data.auth.AuthRepository;
import com.ariefyantobayu.data.auth.remote.AuthRemoteRepository;
import com.ariefyantobayu.data.qrcode.QRCodeDataSource;
import com.ariefyantobayu.data.qrcode.QRCodeRepository;
import com.ariefyantobayu.data.qrcode.remote.QRCodeRemoteRepository;
import com.ariefyantobayu.data.question.QuestionDataSource;
import com.ariefyantobayu.data.question.QuestionRepository;
import com.ariefyantobayu.data.question.remote.QuestionRemoteRepository;

public class Injector {

    public static AuthRepository injectRepository(SharedPreferences sharedPreferences) {
        return new AuthDataSource(new AuthRemoteRepository(sharedPreferences));
    }

    public static QRCodeRepository injectQRCodeRepository(SharedPreferences sharedPreferences) {
        return new QRCodeDataSource(new QRCodeRemoteRepository(sharedPreferences));
    }

    public static QuestionRepository injectQuestionRepository(SharedPreferences sharedPreferences) {
        return new QuestionDataSource(new QuestionRemoteRepository(sharedPreferences));
    }

}
