package com.ariefyantobayu;

import android.content.Context;
import android.content.SharedPreferences;
import android.view.Menu;
import android.view.View;
import android.widget.Toast;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import androidx.appcompat.widget.Toolbar;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.navigation.NavController;
import androidx.navigation.NavGraph;
import androidx.navigation.NavOptions;
import androidx.navigation.Navigation;
import androidx.navigation.ui.AppBarConfiguration;
import androidx.navigation.ui.NavigationUI;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.navigation.NavigationView;
import com.google.android.material.snackbar.Snackbar;

public class NavigationActivity extends AppCompatActivity {

    private static String ROLE_CARETAKER = "CARETAKER";
    private static String ROLE_GENERAL = "GENERAL";
    private static final int[] NAV_CARETAKER = new int[]{
            R.id.nav_dashboard,
            R.id.nav_home,
            R.id.nav_profile,
            R.id.nav_feedback,
            R.id.nav_report
    };
    private static final int[] NAV_GENERAL = new int[] {
            R.id.nav_home,
            R.id.nav_profile,
            R.id.nav_feedback,
    };

    private AppBarConfiguration appBarConfiguration;
    private SharedPreferences sharedPreferences;
    private Toolbar toolbar;
    private FloatingActionButton fab;
    private DrawerLayout drawer;
    private NavigationView navigationView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_navigation);
        sharedPreferences = getSharedPreferences("app", Context.MODE_PRIVATE);

        toolbar = findViewById(R.id.toolbar);
        fab = findViewById(R.id.fab);
        drawer = findViewById(R.id.drawer_layout);
        navigationView = findViewById(R.id.nav_view);
        setSupportActionBar(toolbar);

        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make(view, "Hello Snackbar", Snackbar.LENGTH_SHORT)
                        .setAction("Action", new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                Toast.makeText(NavigationActivity.this, "Hallo Action Snackbar", Toast.LENGTH_SHORT).show();
                            }
                        }).show();
            }
        });

        NavController navController = Navigation.findNavController(this, R.id.nav_host_fragment_content_navigation);
        NavGraph navGraph = navController.getGraph();
        String role = sharedPreferences.getString("role", ROLE_GENERAL);

        appBarConfiguration = new AppBarConfiguration.Builder(NAV_CARETAKER)
                .setOpenableLayout(drawer)
                .build();
        if (role.equalsIgnoreCase(ROLE_CARETAKER)) {
//            navigationView.getMenu().clear();
//            navigationView.inflateMenu(R.menu.caretaker_navigation_drawer);
            navGraph.setStartDestination(R.id.nav_dashboard);
            fab.setVisibility(View.VISIBLE);
        } else {
            navigationView.getMenu().clear();
            navigationView.inflateMenu(R.menu.employee_navigation_drawer);
//            navGraph = navController.getNavInflater().inflate(R.navigation.employee_navigation);
//            fab.setVisibility(View.INVISIBLE);
            navGraph.setStartDestination(R.id.nav_home);
        }

        navController.setGraph(navGraph);
        NavigationUI.setupActionBarWithNavController(this, navController, appBarConfiguration);
        NavigationUI.setupWithNavController(navigationView, navController);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.navigation, menu);
        return true;
    }

    @Override
    public boolean onSupportNavigateUp() {
        NavController navController = Navigation.findNavController(this, R.id.nav_host_fragment_content_navigation);
        return NavigationUI.navigateUp(navController, appBarConfiguration) ||super.onSupportNavigateUp();
    }
}