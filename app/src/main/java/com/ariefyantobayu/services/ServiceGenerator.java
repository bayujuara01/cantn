package com.ariefyantobayu.services;

import com.ariefyantobayu.BuildConfig;
import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

import java.io.IOException;

public class ServiceGenerator {

    private static final Retrofit.Builder retrofitBuilder = new Retrofit.Builder()
            .baseUrl(BuildConfig.SERVER_URL + "/api/")
            .addConverterFactory(GsonConverterFactory.create());

    private static Retrofit retrofit = retrofitBuilder.build();

    private static HttpLoggingInterceptor httpLogging = new HttpLoggingInterceptor()
            .setLevel(HttpLoggingInterceptor.Level.BODY);

    private static OkHttpClient.Builder httpClientBuilder = new OkHttpClient.Builder();

    private static Interceptor authorizationInterceptor(String token) {
        return new Interceptor() {
            @Override
            public Response intercept(Chain chain) throws IOException {
                Request request = chain.request().newBuilder()
                        .addHeader("Authorization", "Bearer " + token)
                        .build();
                return chain.proceed(request);
            }
        };
    }

    private ServiceGenerator() {}

    public static <Service> Service create(Class<Service> classService) {
        if (!httpClientBuilder.interceptors().contains(httpLogging)) {
            httpClientBuilder.addInterceptor(httpLogging);
            retrofitBuilder.client(httpClientBuilder.build());
            retrofit = retrofitBuilder.build();
        }
        return retrofit.create(classService);
    }

    public static <Service> Service createWithAuthorization(Class<Service> classService, String token) {
        if (!httpClientBuilder.interceptors().contains(httpLogging)) {
            httpClientBuilder.addInterceptor(authorizationInterceptor(token));
            httpClientBuilder.addInterceptor(httpLogging);
            retrofitBuilder.client(httpClientBuilder.build());
            retrofit = retrofitBuilder.build();
        }
        return retrofit.create(classService);
    }
}
