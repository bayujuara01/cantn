package com.ariefyantobayu.services;

import com.ariefyantobayu.base.BaseResponse;
import com.ariefyantobayu.data.qrcode.model.QRCodeRequest;
import com.ariefyantobayu.data.qrcode.model.QRCodeResponse;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.POST;

public interface QRCodeService {
    @POST("qr/generate")
    Call<BaseResponse<QRCodeResponse>> generate(@Body QRCodeRequest qrCodeRequest);
}
