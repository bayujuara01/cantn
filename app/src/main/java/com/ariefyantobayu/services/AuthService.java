package com.ariefyantobayu.services;


import com.ariefyantobayu.data.auth.model.Auth;
import com.ariefyantobayu.data.auth.model.AuthResponse;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.POST;

public interface AuthService {
    @POST("auth/login")
    Call<AuthResponse> authenticate(@Body Auth credentials);
}
